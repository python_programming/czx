
import requests
import re
import csv
import jieba
import wordcloud
import PIL.Image as image
import numpy as np
import xlwt

def down_danmu():
    url = "https://api.bilibili.com/x/v1/dm/list.so?oid=186803402"

    r = requests.get(url)

    html = r.content.decode('utf-8')

    res = re.compile('<d.*?>(.*?)</d>')

    barrage = re.findall(res, html)

    for i in barrage:
        with open('./后浪弹幕.csv', 'a', newline='', encoding='utf-8-sig') as f:
            writer = csv.writer(f)
            danmu = []
            danmu.append(i)
            writer.writerow(danmu)
    print("文件写入成功！")



    print("save in excel...")
    workbook = xlwt.Workbook(encoding="utf-8",style_compression=0)
    worksheet = workbook.add_sheet('《后浪》弹幕情况',cell_overwrite_ok=True)
    col = ("弹幕情况",)
    worksheet.write(0,0,col[0])
    for i in range(0,500):
        data = barrage[i]
        worksheet.write(i+1,0,data)
    workbook.save('后浪弹幕.xls')

def showPic():
    f = open('./后浪弹幕.csv',encoding='utf-8')
    txt = f.read()
    txt_list = jieba.lcut(txt)
    # print(txt_list)
    string = " ".join(txt_list)
    print(string)

    #img = np.array(image.open('./bilibili.jpg'))
    img = np.array(image.open('./bili.jpg'))
    w = wordcloud.WordCloud(
        width=1000,
        height=700,
        background_color='white',
        font_path='msyh.ttc',
        mask=img,
        scale=15,
        stopwords={' '},
        contour_color='white',
        contour_width=10,
    )
    w.generate(string)
    w.to_file('./demo4.png')

down_danmu()
showPic()

