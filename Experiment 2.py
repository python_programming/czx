import random

count = 0
right = 0
while True:
    a = random.randint(1, 11)
    b = random.randint(1, 11)
    dict = [["%d+%d=" % (a, b), a + b], ["%d-%d=" % (a, b), a - b], ["%d*%d=" % (a, b), a * b],
            ["%d/%d=" % (a, b), a // b], ["%d%%%d=" % (a,b), a % b]]
    i = random.randint(0, 4)
    num = input("%s? input your answer(除法结果只取商)(结束请输入q)" % (dict[i][0]))
    if num == "q":
        break
    elif int(num) == dict[i][1]:
        count += 1
        right += 1
        print("right")
    else:
        count += 1
        print("wrong")

print("total is %d\nright nums is %d\n right rate is %d\n" % (count, right, right / count))

