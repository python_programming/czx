import pymysql

db = pymysql.connect('localhost','root','czx990919','xuanxuan',charset = 'utf8')

cursor = db.cursor()
# cursor.execute('create database xuange;')
#cursor.execute('use xuange;')
cursor.execute('drop table if EXISTS heroes;')
cursor.execute('create table if not exists heroes (number int NOT NULL primary key, name varchar(20), location varchar(20));')
cursor.execute('insert into heroes (number, name, location) values (20192217, "轩宣萱璇哥", "打野");')
cursor.execute('insert into heroes (number, name, location) values (20192216, "轩宣萱璇姐", "辅助");')

cursor.execute('update heroes set location = "边路" where number = 20192217;')
cursor.execute('delete from heroes where number = 20192216;')
cursor.execute('select * from heroes;')
data = cursor.fetchall()
print(data)

cursor.close()
db.close()

# import sqlite3
#
# conn = sqlite3.connect("besti1922.db")
# cursor = conn.cursor()
#
# #创建一个表
# cursor.execute('create table if not exists student (number int(10) primary key, name varchar(20), score float)')
# cursor.execute('insert into student (number, name, score) values (20192217, "轩宣萱璇哥", 100)')
# cursor.execute('select * from student')
# print(cursor.fetchall())
# conn.close()

# import pymysql
# #建库
# try:
#     conn=pymysql.connect(
#         host='127.0.0.1',
#         port=3306,
#         user='root',
#         passwd='czx990919',
#     )
#     cur=conn.cursor()
#     create_database_sql='CREATE DATABASE IF NOT EXISTS czxhouse DEFAULT CHARSET utf8 COLLATE utf8_general_ci;'
#     cur.execute(create_database_sql)
#     cur.close()
#     print('创建数据库 czxhouse 成功！')
# except pymysql.Error as e:
#     print('pymysql.Error: ',e.args[0],e.args[1])
#
# # 建表
# try:
#     conn=pymysql.connect(
#         host='127.0.0.1',
#         port=3306,
#         user='root',
#         passwd='czx990919',
#         db='czxhouse',
#         charset='utf8'
#     )
#     cur=conn.cursor()
#     cur.execute('drop table if exists user;')
#     create_table_sql='''
#         CREATE TABLE user(
#             id int(11) DEFAULT NULL ,
#             name VARCHAR(50) DEFAULT NULL ,
#             password VARCHAR(30) DEFAULT NULL ,
#             birthday TIMESTAMP DEFAULT now()
#         )engine=innodb DEFAULT CHARACTER set utf8;
#     '''
#     cur.execute(create_table_sql)
#     print('创建数据库表成功！')
# except pymysql.Error as e:
#     print('mysql.Error: ',e.args[0],e.args[1])
# import pymysql
# db = pymysql.connect(host='localhost',
#                           user='root',
#                           password='czx990919')
# cursor = db.cursor()
# cursor.execute('create database 20192213')
# cursor.close()
# db.close()
#
# db = pymysql.connect(host = '127.0.0.1',port = 3306,user = 'root',password = 'lzq2001',database = '20192213')
# cursor = db.cursor()
#
# #建表
# cursor.execute('create table if not exists 20192213(number int(5) primary key, name varchar(20))')
# #插入数据
# cursor.execute("insert into 20192213 (number,name) select 001,'刘子谦' ")
# cursor.execute("insert into 20192213(number,name) select 002,'程子轩'")
# #查询
# cursor.execute('select * from 20192213')
# print(cursor.fetchall())
# #修改
# cursor.execute("update 20192213 set name = '谢浩翔' where number = 002")
# #再次查询
# cursor.execute('select * from 20192213')
# print(cursor.fetchall())
# #删除
# cursor.execute('delete from 20192213 where number = 001')
# #再次查询
# #再次查询
# cursor.execute('select * from 20192213')
# print(cursor.fetchall())
# cursor.close()
# db.close()