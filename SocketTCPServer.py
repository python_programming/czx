import socket

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

s.bind(('127.0.0.1',8001))
s.listen()
conn, address = s.accept()
data = conn.recv(1024)

print(data.decode())
conn.sendall(('服务器已收到数据内容：'+str(data)).encode())
s.close()