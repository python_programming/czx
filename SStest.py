import os
from socket import *
import base64

HOST = '127.0.0.1'  #对bind（）方法的标识，表示可以使用任何可用的地址
PORT = 8001  #设置端口
BUFSIZ = 1024  #设置缓存区的大小
ADDR = (HOST, PORT)

tcpSerSock = socket(AF_INET, SOCK_STREAM)  #定义了一个套接字
tcpSerSock.bind(ADDR)  #绑定地址
tcpSerSock.listen(5)     #规定传入连接请求的最大数，异步的时候适用

while True:
    print('waiting for connection...')
    tcpCliSock, addr = tcpSerSock.accept()
    print ('...connected from:', addr)
    while True:
        data = tcpCliSock.recv(BUFSIZ)           #收一
        print("recv:",data.decode("utf-8"))
        if not data:
            break
        filename = data.decode("utf-8")
        if os.path.exists(filename):
            filesize = str(os.path.getsize(filename))
            print("文件大小为：",filesize)
            tcpCliSock.send(filesize.encode())                                            #发一
            data = tcpCliSock.recv(BUFSIZ)   #挂起服务器发送，确保客户端单独收到文件大小数据，避免粘包    #收二
            print("开始发送")
            f = open(filename, "rb+")
            container1 = f.read()
            f.seek(0)
            f.truncate()
            container2 = base64.b64encode(container1)
            f.write(container2)
            f.close()
            f1 = open(filename,'rb+')
            fina = f1.read()
            tcpCliSock.send(fina)                           #发二
            print('发送成功')
        else:
            tcpCliSock.send("0001".encode())   #如果文件不存在，那么就返回该代码
    tcpCliSock.close()
tcpSerSock.close()