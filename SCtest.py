from socket import *
import base64

HOST = '127.0.0.1'
PORT = 8001
BUFSIZ = 1024
ADDR = (HOST, PORT)

tcpCliSock = socket(AF_INET, SOCK_STREAM)
tcpCliSock.connect(ADDR)

while True:
    message = input('> ')  #输入你的文件名
    if not message:
        break
    tcpCliSock.send(bytes(message, 'utf-8'))    #发一
    data = tcpCliSock.recv(BUFSIZ) #接文件大小                      #收一
    if not data:
        break
    if data.decode() == "0001":
        print("Sorry file %s not found"%message)
    else:
        tcpCliSock.send("File size received".encode())             #发二
        file_total_size = int(data.decode())
        received_size = 0
        f = open("new" + message  ,"wb")
        data2 = tcpCliSock.recv(BUFSIZ)         # 这一步接不到数据               #收二
        data3 = base64.b64decode(data2)
        print(data3)
        f.write(data3)
        received_size += len(data3)
        print("已接收:",received_size)
        f.close()
        print("receive done",file_total_size," ",received_size)
tcpCliSock.close()